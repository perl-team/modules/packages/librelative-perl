Source: librelative-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libuniversal-require-perl,
                     libtest-pod-perl,
                     libtest-pod-coverage-perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/librelative-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/librelative-perl.git
Homepage: https://metacpan.org/release/relative

Package: librelative-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libuniversal-require-perl
Multi-Arch: foreign
Description: Load modules with relative names
 This module allows you to load modules using only parts of their name,
 relatively to the current module or to a given module. Module names are
 by default searched below the current module, but can be searched upper
 in the hierarchy using the ..:: syntax.
 .
 In order to further loosen the namespace coupling, import returns
 the full names of the loaded modules, making object-oriented code easier
 to write:
 .
    use relative;
 .
    my ($Maker, $Publisher) = import relative qw(Create Publish);
    my $report    = $Maker->new;
    my $publisher = $Publisher->new;
